# Screenshot
![output.gif](./output.gif)

## Inspired from this video
https://www.youtube.com/watch?v=ArTVfdHOB-M

# Installation

```bash
git clone https://codeberg.org/anhsirk0/tailwind-navbar.git
```

```bash
cd tailwind-navbar
```

# Running the app

## First to download all dependencies run (in the project root , where package.json is located)
```bash
npm install
```

## Finally start the server via
```bash
npm start
```

